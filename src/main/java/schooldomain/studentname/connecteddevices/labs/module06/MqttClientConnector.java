package schooldomain.studentname.connecteddevices.labs.module06;

import java.util.logging.Level;
import java.util.logging.Logger;
import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;
import com.labbenchstudios.edu.connecteddevices.common.SensorData;
import com.labbenchstudios.edu.connecteddevices.common.ConfigConst;
import com.labbenchstudios.edu.connecteddevices.common.DataUtil;

public class MqttClientConnector implements MqttCallback {
	/*
	 * MqttClientConnector class implements the following methods
	 * 
	 * a) MqttClientConnector() b) connect() c) disconnect() d) publishMessage() e)
	 * subscribeToTopic() f) unSubscibe() g) message() h) connectionLost() i)
	 * messageArrived() j) deliveryComplete()
	 * 
	 * @param: logger --> Logger object is used to log messages for a specific
	 * system or application component.
	 * 
	 * @param: protocol --> MQTT Protocol used in connection
	 * 
	 * @param: host --> domain address of MQTT broker
	 * 
	 * @param: port --> Detailed port address of MQTT broker
	 * 
	 * @param: clientID --> Depicts Client id
	 * 
	 * @param: sensorData --> instance of SensorData object
	 * 
	 * @param: dataUtil --> instance of DataUtil object
	 */
	private static final Logger logger = Logger.getLogger(MqttClientConnector.class.getName());
	private String protocol = ConfigConst.DEFAULT_MQTT_PROTOCOL;
	private String host = ConfigConst.DEFAULT_MQTT_SERVER;
	private int port = ConfigConst.DEFAULT_MQTT_PORT;
	private String clientID;
	private String brokerAddr;
	private MqttClient mqttClient;
	private SensorData sensorData;
	private DataUtil dataUtil;

	/**
	 * Default Constructor
	 */
	public MqttClientConnector() {

		if (host != null && host.trim().length() > 0) {

			this.sensorData = new SensorData();
			this.dataUtil = new DataUtil();
			this.clientID = MqttClient.generateClientId();
			logger.info("Using client id for broker connection: " + clientID);
			this.brokerAddr = protocol + "://" + host + ":" + port;
			logger.info("Using URL for broker connection: " + brokerAddr);

		}

	}

	/**
	 * Establish a connection to MQTT broker
	 */
	public void connect() {

		if (mqttClient == null) {

			MemoryPersistence persistence = new MemoryPersistence();
			try {
				// MqqtClient object
				mqttClient = new MqttClient(brokerAddr, clientID, persistence);
				// MqttConnectOptions object holds set of options that control how client
				// connects
				MqttConnectOptions connOpts = new MqttConnectOptions();
				connOpts.setCleanSession(true);
				logger.info("Connecting to broker: " + brokerAddr);
				// setting the callback
				mqttClient.setCallback(this);
				// connecting to broker
				mqttClient.connect(connOpts);
				logger.info("connected to broker: " + brokerAddr);
			}

			catch (MqttException ex) {

				logger.log(Level.SEVERE, "Failed to connect Broker" + brokerAddr, ex);

			}
		}
	}

	/**
	 * Disconnect from MQTT broker
	 */
	public void disconnect() {

		try {

			mqttClient.disconnect();
			logger.info("Disconnect from broker: " + brokerAddr);
		}

		catch (Exception ex) {

			logger.log(Level.SEVERE, "Failed to disconnect from broker: " + brokerAddr, ex);

		}
	}

	/**
	 * Publish_My_Message Method
	 * 
	 * @param: topic --> topic to publish a custom defined message
	 * @param: qosLevel --> setting the QOS level
	 * @param: payload --> Actual Message to be sent
	 */
	public boolean Publish_My_Message(String topic, int qosLevel, byte[] payload) {

		boolean msgSent = false;

		try {

			logger.info("Publishing message to topic: " + topic);
			// MqttMessage object
			MqttMessage msg = new MqttMessage(payload);
			msg.setQos(qosLevel);
			// publish the message to topic
			mqttClient.publish(topic, msg);
			logger.info("Message Published " + msg.getId());
			logger.info(payload.toString());
			msgSent = true;
		}

		catch (Exception ex) {

			logger.log(Level.SEVERE, "Failed to Publish Mqtt Message " + ex.getMessage());

		}

		return msgSent;
	}

	/**
	 * SubscribeToTopic Method
	 * 
	 * @param: topic --> topic name to subscribe to
	 */
	public boolean SubscribeToTopic(String topic) {

		boolean success = false;

		try {

			mqttClient.subscribe(topic);
			success = true;
		}

		catch (MqttException e) {

			e.printStackTrace();

		}

		return success;

	}

	/**
	 * unSubscibe Method to Unsubscribe from a topic
	 */
	public boolean unSubscibe(String topic) {

		boolean success = false;

		try {

			// Unsubscribe from the Topic
			mqttClient.unsubscribe(topic);
			success = true;
		}

		catch (MqttException e) {

			e.printStackTrace();

		}

		return success;

	}

	/**
	 * Message to Publish in JSON
	 */
	public byte[] message() {

		sensorData.setCurValue(30);
		sensorData.setAvgValue(25);
		sensorData.setMaxValue(40);
		sensorData.setMinValue(6);
		sensorData.setSamples(6);
		sensorData.setTimeStamp("time");
		sensorData.setTotValue(250);

		// Convert SensorData to JSON
		String jsonMsg = dataUtil.SensorDataToJson(sensorData);
		return jsonMsg.getBytes();
	}

	/**
	 * Required CallBacks
	 */
	@Override
	public void connectionLost(Throwable cause) {

		logger.log(Level.WARNING, "Connection Lost. Come back Later !!!", cause);

	}

	/**
	 * messageArrived() method called when a certain message arrives from actual
	 * publisher
	 */
	@Override
	public void messageArrived(String topic, MqttMessage message) throws Exception {

		logger.info("Message arrived: " + topic + "," + message.getId());
		SensorData senmsg = dataUtil.JsonToSensorData(message.toString());
		logger.info("SensorData Message: \n" + senmsg);
		logger.info("JSON Message: " + dataUtil.SensorDataToJson(senmsg));

	}

	/**
	 * deliveryComplete() method called when there is successful message published
	 */
	@Override
	public void deliveryComplete(IMqttDeliveryToken token) {

		logger.info("Delivery Complete: " + token.getMessageId() + "-" + token.getResponse());

	}
}
