package schooldomain.studentname.connecteddevices.labs.module06;

import java.util.logging.Logger;

public class MqttPubClientTestApp {
	/**
	 * class to Publish message to MQTT client
	 * 
	 * @param : logger --> Logger object is used to log messages for a specific
	 *        system or application component.
	 * @param: app --> reference to this class
	 * @param: mqqtClient --> reference to MqttClientConnector
	 */
	private static final Logger logger = Logger.getLogger(MqttSubClientTestApp.class.getName());
	private static MqttPubClientTestApp app;
	private MqttClientConnector mqttClient;

	/**
	 * Constructor
	 */
	public MqttPubClientTestApp() {

		super();

	}

	/**
	 * @param : topicName --> the name of topic to publish this method publishes
	 *        message to a topic and then disconnects
	 */
	public void start(String topicName) {

		mqttClient = new MqttClientConnector();
		mqttClient.connect();
		mqttClient.Publish_My_Message(topicName, 2, mqttClient.message());

		try {

			Thread.sleep(30000); // sleep for 30 seconds
		}

		catch (InterruptedException e) {

			e.printStackTrace();

		}

		mqttClient.disconnect();
	}

	public static void main(String[] args) {

		app = new MqttPubClientTestApp();

		try {

			app.start("Publish Client Message");
		}

		catch (Exception ex) {

			ex.printStackTrace();

		}

	}

}
