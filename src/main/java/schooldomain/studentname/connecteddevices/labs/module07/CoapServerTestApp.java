package schooldomain.studentname.connecteddevices.labs.module07;

public class CoapServerTestApp {
	/*
	 * @App --> CoapServerTestApp --> Instance Variable
	 * 
	 * @coapServer --> CoapServerConnector class instance variable
	 */
	private static CoapServerTestApp App;
	private CoapServerConnector coapServer;

	/**
	 * default constructor
	 */
	public CoapServerTestApp() {
		super();
	}

	/**
	 * CoapServer Start method
	 */
	public void start() {
		coapServer = new CoapServerConnector();
		coapServer.start();
	}

	/**
	 * Java Class Main Method to run the program
	 */
	public static void main(String[] args) {
		App = new CoapServerTestApp();
		try {
			App.start();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
