package schooldomain.studentname.connecteddevices.labs.module07;

public class CoapClientTestApp {
	/*
	 * @var App --> instance variable for CoapClientTestApp
	 * 
	 * @var coapClient --> instance variable for CoapClientConnector
	 */
	private static CoapClientTestApp App;
	private CoapClientConnector coapClient;

	/**
	 * default constructor
	 */
	public CoapClientTestApp() {
		super();
	}

	/**
	 * initialize the CoapClientConnector and send requests
	 */
	public void Start() {
		coapClient = new CoapClientConnector();
		coapClient.runTests("temperature");
	}

	/**
	 * main method
	 */
	public static void main(String[] args) {

		App = new CoapClientTestApp();
		try {
			App.Start();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}
}
