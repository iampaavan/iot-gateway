/*
Created on February 14, 2019
@author: Paavan Gopala
*/


package schooldomain.studentname.connecteddevices.labs.module05;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

import com.labbenchstudios.edu.connecteddevices.common.DataUtil;
import com.labbenchstudios.edu.connecteddevices.common.SensorData;

/* 
TempManagementApp class which drives the 
Java program to yield Emulated Temperature
Sensor Readings.
*/
public class TempManagementApp {

	// Sensor Data class variables
	SensorData sensorData;
	// Data-Util class variables
	DataUtil dataUtil;
	// File-path stored of type String 
	String filePath;
	// JSON data of type String
	String JSON_DATA;

	/*
	 * Default Constructor
	 */
	public TempManagementApp() {
		// instance of SensorData
		this.sensorData = new SensorData();
		// instance of DataUtil
		this.dataUtil = new DataUtil();
		// read the JSON contents from the below mentioned file-path
		this.filePath = "C:/Users/Paavan Gopala/Desktop/sensordata.txt";
		// create an instance of JSON_DATA of type String
		this.JSON_DATA = new String();
	}

	/*
	 * Method to read the JSON Data from the 
	 * text file
	 */
	public String ReadJsonFromFile() {
		try {
			String json_data = new String(Files.readAllBytes(Paths.get(filePath)));
			return json_data;
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
	}
	
	/*
	 * Method to print the Sensor Data
	 * in JSON format
	 */
	public void PrintSensorData() {
		sensorData = dataUtil.JsonToSensorData(ReadJsonFromFile());
		System.out.println("\n");
		System.out.println("Emulated Sensor Data: \n" + sensorData);
	}

	/*
	 * Method to read the Sensor Data
	 * and print in JSON format
	 */
	public void JSON_Data() {
		sensorData = dataUtil.JsonToSensorData(ReadJsonFromFile());
		JSON_DATA = dataUtil.SensorDataToJson(sensorData);
		System.out.println("\n");
		System.out.println("Json Data: " + JSON_DATA);

	}

	/*
	 * Main Method of Java to print the Sensor Data
	 * in the original format
	 */
	public static void main(String[] args) {

		TempManagementApp temp_mgmt_app = new TempManagementApp();
		String result = temp_mgmt_app.ReadJsonFromFile();
		System.out.println("Emulated Temperature Readings: \n");
		System.out.println(result);

		temp_mgmt_app.PrintSensorData();
	}

}
