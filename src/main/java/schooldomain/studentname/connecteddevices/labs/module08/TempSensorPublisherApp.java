package schooldomain.studentname.connecteddevices.labs.module08;

import java.util.Random;
import java.util.logging.Logger;
import com.labbenchstudios.edu.connecteddevices.common.ConfigConst;
import schooldomain.studentname.connecteddevices.labs.module08.MqttClientConnector;

/**
 * Created on Feb 24, 2019 TempSensorPublisherApp.java: Java class to publish
 * message using MQTT protocol
 * 
 * @author paavan gopala
 */

public class TempSensorPublisherApp {

	/**
	 * class to publish message using MQTT protocol to Ubidots cloud
	 * 
	 * @var pubApp: MQTT publisher class self instance
	 * @var mqttClient: MQTT connector helper class instance
	 * @var host: Broker address for Mqtt
	 * @var UBIDOTS_VARIABLE_LABEL: string constant for ubidots variable name
	 * @var UBIDOTS_VARIABLE_LABEL: String constant for ubidots Device name
	 * @var UBIDOTS_TOPIC_DEFAULT: Topic name to subscribe/publish
	 * @var pemFileName: file location of the Ubidots pem file
	 */
	private static final Logger logger = Logger.getLogger(TempSensorPublisherApp.class.getName());
	private static TempSensorPublisherApp pubApp;
	private String host = ConfigConst.DEFAULT_UBIDOTS_SERVER;
	private String pemFileName = "C:\\Users\\Paavan Gopala\\git\\connected-devices-java\\src\\main\\java\\com\\labbenchstudios\\edu\\connecteddevices\\common" + ConfigConst.UBIDOTS
			+ ConfigConst.CERT_FILE_EXT;
	private String authToken = "A1E-35snJMk1KIRXsvZVwYhvpRgL7tswQd";
	private MqttClientConnector mqttClient;
	public static final String UBIDOTS_VARIABLE_LABEL = "/TempSensor";
	public static final String UBIDOTS_DEVICE_LABEL = "/my_ubidots_device";
	public static final String UBIDOTS_TOPIC_DEFAULT = "/v1.6/devices" + UBIDOTS_DEVICE_LABEL + UBIDOTS_VARIABLE_LABEL;

	/**
	 * MqttPubClientTestApp Constructor
	 */
	public TempSensorPublisherApp() {
		super();
	}

	/**
	 * method to connect mqqt client to broker and publish the message
	 * @param topicName: name of the MQTT session topic in string
	 */
	public void start(String topicName) {
		
			try {
				mqttClient = new MqttClientConnector(host, authToken, pemFileName);
				mqttClient.connect();
				while (true) {
					mqttClient.publishMessage(topicName, ConfigConst.DEFAULT_QOS_LEVEL,
						generateRandomvalue(0.0f, 40.0f).getBytes());
					Thread.sleep(60000);
				}
			} catch (Exception e) {				
				mqttClient.disconnect();
				e.printStackTrace();
			}
		}

	/**
	 * Ramdomly generates floating point number between 
	 * min and max value to emulate sensor value
	 * @param min: minimum value to be generated
	 * @param max: maximum value to be generated
	 */
	public String generateRandomvalue(float min, float max) {
		Random r = new Random();
		float random = min + r.nextFloat() * (max - min);
		logger.info("\n\n Temperature Reading : " + Float.toString(random) + "\n");
		return Float.toString(random);
	}

	/**
	 * Main function of MQTT publisher class
	 */
	public static void main(String[] args) {
		pubApp = new TempSensorPublisherApp();
		
			pubApp.start(UBIDOTS_TOPIC_DEFAULT);
	}
}
