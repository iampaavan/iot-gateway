package schooldomain.studentname.connecteddevices.labs.module08;

import com.labbenchstudios.edu.connecteddevices.common.ConfigConst;
import schooldomain.studentname.connecteddevices.labs.module08.MqttClientConnector;

/**
 * Created on Feb 24, 2019 TempActuatorSubscriberApp.java: Java class to receive
 * message using MQTT protocol
 * 
 * @author paavan gopala
 */
 
public class TempActuatorSubscriberApp {

	/**
	 * Receive message using MQTT protocol (subscribes to Mqtt Topic) 
	 * 
	 * @var subscriberApp: instance variable for TempActuatorSubscriberApp
	 * @var mqttClient: instance variable for MqqtClientConnector
	 * @var host: Broker address for Mqtt
	 * @var pemfileName: file location of the Ubidots pem file
	 * @var UBIDOTS_VARIABLE_LABEL: string constant for ubidots variable name
	 * @var UBIDOTS_VARIABLE_LABEL: String constant for ubidots Device name
	 * @var UBIDOTS_TOPIC_DEFAULT: Topic name to subscribe/publish
	 */
	private static TempActuatorSubscriberApp subscriberApp;
	private MqttClientConnector mqttClient;
	private String host = ConfigConst.DEFAULT_UBIDOTS_SERVER;
	private String pemFileName = "C:\\Users\\Paavan Gopala\\git\\connected-devices-java\\src\\main\\java\\com\\labbenchstudios\\edu\\connecteddevices\\common" + ConfigConst.UBIDOTS
			+ ConfigConst.CERT_FILE_EXT;
	private String authToken = "A1E-FbAuz5bMdf9fZgOk18ddsB1wvWtvAe";
	public static final String UBIDOTS_VARIABLE_LABEL = "/TempActuator";
	public static final String UBIDOTS_DEVICE_LABEL = "/my_ubidots_device";
	public static final String UBIDOTS_TOPIC_DEFAULT = "/v1.6/devices" + UBIDOTS_DEVICE_LABEL + UBIDOTS_VARIABLE_LABEL;

	/**
	 * MqttSubClientTestApp constructor
	 */
	public TempActuatorSubscriberApp() {
		super();
	}

	/**
	 * method to connect mqqt client to broker and publish the message
	 * @param topicName: name of the MQTT session topic in string
	 * 
	 */
	public void start(String topicName) {
		try {
			mqttClient = new MqttClientConnector(host, authToken, pemFileName);
			mqttClient.connect();
			while (true) {
				mqttClient.subscribeToTopic(topicName);
				Thread.sleep(30000);
			}
		} catch (Exception e) {
			e.printStackTrace();
			mqttClient.disconnect();
		}
	}

	/**
	 * Main function of TempActuatorSubscriber class
	 */
	public static void main(String[] args) {

		subscriberApp = new TempActuatorSubscriberApp();
		try {
			subscriberApp.start(UBIDOTS_TOPIC_DEFAULT);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}
}
