package schooldomain.studentname.connecteddevices.labs.module07;

import java.util.logging.Logger;
import org.eclipse.californium.core.CoapResource;
import org.eclipse.californium.core.coap.CoAP.ResponseCode;
import org.eclipse.californium.core.server.resources.CoapExchange;
import com.labbenchstudios.edu.connecteddevices.common.DataUtil;
import com.labbenchstudios.edu.connecteddevices.common.SensorData;

public class TempResourceHandler extends CoapResource {
	/*
	 * @var logger
	 * 
	 * @var sensorData --> SensorData Class Instance Variable
	 * 
	 * @var DataUtil --> DataUtil Class Instance Variable
	 */
	private static final Logger logger = Logger.getLogger(TempResourceHandler.class.getName());
	private SensorData sensorData = new SensorData();
	private DataUtil dataUtil = new DataUtil();

	/**
	 * Default Constructor
	 */
	public TempResourceHandler() {
		super("temperature", true);
	}

	/*
	 * Responds to GET REQUEST
	 */
	@Override
	public void handleGET(CoapExchange ce) {
		ce.respond(ResponseCode.VALID, "GET worked!");
		logger.info("Received GET request from client.");
	}

	/*
	 * Responds to POST REQUEST
	 */
	@Override
	public void handlePOST(CoapExchange ce) {
		ce.respond(ResponseCode.CREATED);
		logger.info("Received POST request from client.");
		String recievedJson = new String(ce.getRequestPayload());
		logger.info("Recieved \n" + recievedJson);
		sensorData = dataUtil.JsonToSensorData(recievedJson);
		logger.info("\n" + sensorData.toString());
		logger.info("POST successfull \n");
	}

	/*
	 * Responds to PUT REQUEST
	 */
	@Override
	public void handlePUT(CoapExchange ce) {
		ce.respond(ResponseCode.CHANGED);
		String recievedjson = new String(ce.getRequestPayload());
		logger.info("update: \n" + recievedjson);
		sensorData = dataUtil.JsonToSensorData(recievedjson);
		logger.info("\n" + sensorData.toString());
		logger.info("PUT Request was successful.\n");
	}

	/*
	 * Responds to DELETE REQUEST
	 */
	@Override
	public void handleDELETE(CoapExchange ce) {
		ce.respond(ResponseCode.DELETED);
		logger.info("Received DELETE  request from client.");
	}
}
