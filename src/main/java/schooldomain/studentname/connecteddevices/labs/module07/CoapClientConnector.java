package schooldomain.studentname.connecteddevices.labs.module07;

import java.util.logging.Level;
import java.util.logging.Logger;
import org.eclipse.californium.core.CoapClient;
import org.eclipse.californium.core.CoapResponse;
import org.eclipse.californium.core.coap.MediaTypeRegistry;
import com.labbenchstudios.edu.connecteddevices.common.ConfigConst;
import com.labbenchstudios.edu.connecteddevices.common.DataUtil;
import com.labbenchstudios.edu.connecteddevices.common.SensorData;

public class CoapClientConnector {
	/*
	 * @var protocol --> the protocol used
	 * 
	 * @var host --> IP/domain of the host
	 * 
	 * @var serverAddr --> Server Class Address
	 * 
	 * @var clientConn --> COAP client class instance variable
	 * 
	 * @var sensorData --> SensorData class instance variable
	 * 
	 * @var dataUtil --> DataUtil class instance variable
	 */
	private static final Logger logger = Logger.getLogger(CoapClientConnector.class.getName());
	private String protocol;
	private String host;
	private String serverAddr;
	private CoapClient clientConn;
	private SensorData sensorData;
	private DataUtil dataUtil;

	/**
	 * Default Constructor
	 *
	 */
	public CoapClientConnector() {
		this(ConfigConst.DEFAULT_COAP_SERVER);
	}

	/**
	 * Parameterized Constructor
	 *
	 * @param host
	 */
	public CoapClientConnector(String host) {
		super();
		this.protocol = ConfigConst.DEFAULT_COAP_PROTOCOL;
		if (host != null && host.trim().length() > 0) {
			this.host = host;
		} else {
			this.host = ConfigConst.DEFAULT_COAP_SERVER;
		}
		this.serverAddr = this.protocol + "://" + this.host;
		logger.info("Using URL for server conn: " + this.serverAddr);
		sensorData = new SensorData();
		dataUtil = new DataUtil();
	}

	/**
	 * COAP Client Initialization
	 * 
	 * @param resourceName
	 */
	private void initClient(String resourceName) {

		if (clientConn != null) {
			clientConn.shutdown();
			clientConn = null;
		}
		try {
			if (resourceName != null) {
				serverAddr += "/" + resourceName;
			}
			clientConn = new CoapClient(serverAddr);
			logger.info("Created client connection to server / resource: " + serverAddr);
		} catch (Exception e) {
			logger.log(Level.SEVERE, "Failed to connect to broker: " + getCurrentUri(), e);
		}
	}

	/**
	 * Run GET Run POST Run PUT Run DELETE
	 * 
	 * @param resourceName
	 */
	public void runTests(String resourceName) {
		try {
			initClient(resourceName);
			logger.info("Current URI: " + getCurrentUri());
			sensorData.addValue(4.00f);
			pingServer();
			sendGetRequest();
			sendPostRequest(dataUtil.SensorDataToJson(sensorData));
			sendGetRequest();
			sensorData.addValue(13.00f);
			sendPutRequest(dataUtil.SensorDataToJson(sensorData));
			sendGetRequest();
			sendDeleteRequest();
			pingServer();
		} catch (Exception e) {
			logger.log(Level.SEVERE, "Failed to issue request to CoAP server.", e);
		}
	}

	/**
	 * Returns the CoAP client URI (if set, otherwise returns the serverAddr, or
	 * null).
	 *
	 * @return String
	 */
	public String getCurrentUri() {
		return (this.clientConn != null ? this.clientConn.getURI() : this.serverAddr);
	}

	/**
	 * Server --> Pinging
	 */
	public void pingServer() {
		logger.info("Sending ping...");
		clientConn.ping();
	}

	/**
	 * Send DELETE Request
	 */
	public void sendDeleteRequest() {
		logger.info("Sending Delete request...");
		CoapResponse response = null;
		response = clientConn.delete();
		if (response != null) {
			logger.info(
					"Response: " + response.isSuccess() + " - " + response.getOptions() + " - " + response.getCode());
		} else {
			logger.warning("No response received.");
		}
	}

	/**
	 * Send GET Request
	 */
	public void sendGetRequest() {
		logger.info("Sending Get request...");
		CoapResponse response = null;
		response = clientConn.get();
		if (response != null) {
			logger.info(
					"Response: " + response.isSuccess() + " - " + response.getOptions() + " - " + response.getCode());
		} else {
			logger.warning("No response received.");
		}
	}

	/**
	 * Send POST Request
	 * 
	 * @param payload --> the message to be sent as POST request
	 */
	public void sendPostRequest(String payload) {
		logger.info("Sending Post request...");
		CoapResponse response = null;
		logger.info("Sensor Data: " + payload);
		response = clientConn.post(payload, MediaTypeRegistry.TEXT_PLAIN);
		if (response != null) {
			logger.info(
					"Response: " + response.isSuccess() + " - " + response.getOptions() + " - " + response.getCode());
		} else {
			logger.warning("No response received.");
		}
	}

	/**
	 * Send PUT Request
	 * 
	 * @param payload --> the message to be sent as POST request
	 */
	public void sendPutRequest(String payload) {
		logger.info("Sending Put request...");
		CoapResponse response = null;
		logger.info("Sensor Data: " + payload);
		response = clientConn.put(payload, MediaTypeRegistry.TEXT_PLAIN);
		if (response != null) {
			logger.info(
					"Response: " + response.isSuccess() + " - " + response.getOptions() + " - " + response.getCode());
		} else {
			logger.warning("No response received.");
		}
	}
}
