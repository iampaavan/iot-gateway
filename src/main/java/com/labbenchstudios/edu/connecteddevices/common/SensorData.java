package com.labbenchstudios.edu.connecteddevices.common;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;

public class SensorData implements Serializable {

	// private variables
	private static final long serialVersionUID = 1L;

	public String getTimeStamp() {
		return timeStamp;
	}

	public void setTimeStamp(String timeStamp) {
		this.timeStamp = timeStamp;
	}

	public float getCurValue() {
		return curValue;
	}

	public void setCurValue(float curValue) {
		this.curValue = curValue;
	}

	public float getTotValue() {
		return totValue;
	}

	public void setTotValue(float totValue) {
		this.totValue = totValue;
	}

	public int getSamples() {
		return samples;
	}

	public void setSamples(int samples) {
		this.samples = samples;
	}

	public void setAvgValue(float avgValue) {
		this.avgValue = avgValue;
	}

	public void setMinValue(float minValue) {
		this.minValue = minValue;
	}

	public void setMaxValue(float maxValue) {
		this.maxValue = maxValue;
	}

	public String getName() {
		return Name;
	}

	public void setName(String name) {
		Name = name;
	}

	private String timeStamp = null;
	private String Name = null;
	private float curValue = 0.0f;
	private float avgValue = 0.0f;
	private float minValue = 0.0f;
	private float maxValue = 0.0f;
	private float totValue = 0.0f;
	private int samples = 0;

	/*
	 * Default constructor.
	 */
	public SensorData() {
		super();
		updateTimeStamp();
	}

	/*
	 * addValue method gets the real time value and adds to @param: curValue
	 * and @param: totValue every-time new value comes in.
	 * 
	 * @param val
	 */
	public void addValue(float val) {
		updateTimeStamp();
		++samples;
		curValue = val;
		totValue += val;
		if (curValue < minValue) {
			minValue = curValue;
		}
		if (curValue > maxValue) {
			maxValue = curValue;
		}
		if (totValue != 0 && samples > 0) {
			avgValue = totValue / samples;
		}
	}

	/*
	 * Method to get the Average Temperature
	 * 
	 * @param: avgValue --> Stores avg. temp.
	 */
	public float getAvgValue() {
		return avgValue;
	}

	/*
	 * Method to get the Maximum Temperature
	 * 
	 * @param: avgValue --> Stores max. temp.
	 */
	public float getMaxValue() {
		return maxValue;
	}

	/*
	 * Method to get the Minimum Temperature
	 * 
	 * @param: minValue --> Stores min. temp.
	 */
	public float getMinValue() {
		return minValue;
	}

	/*
	 * Method to get the Current Temperature
	 * 
	 * @param: minValue --> Stores cur. temp.
	 */
	public float getValue() {
		return curValue;
	}

	/*
	 * Private Method for getting the current Time-Stamp
	 */
	private void updateTimeStamp() {
		timeStamp = new SimpleDateFormat("yyyy.MM.dd HH:mm.ss").format(new Date());
	}

	/*
	 * toString() method to convert the data from Non-String format to String format
	 */
	public String toString() {
		String st;
		st = ("time: " + timeStamp + "\n" + "Current Temperature: " + curValue + "\n" + "Average Temperature: "
				+ avgValue + "\n" + "Sample number: " + samples + "\n" + "Min: " + minValue + "\n" + "Max: " + maxValue
				+ "\n");
		return st;
	}
}