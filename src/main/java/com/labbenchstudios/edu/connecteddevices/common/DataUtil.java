package com.labbenchstudios.edu.connecteddevices.common;

import com.google.gson.Gson;

/*
 * DataUtil Class
 */
public class DataUtil {

	/*
	 * Method to convert SensorData to JSON
	 * 
	 * @param:jsonSd --> Store the sensor related in JSON format
	 */
	public String SensorDataToJson(SensorData sensordata) {
		String jsonSd;
		Gson gson = new Gson();
		jsonSd = gson.toJson(sensordata);
		return jsonSd;
	}

	/*
	 * Method to convert data in JSON to Sensor Data in String format
	 * 
	 * @param: sensorData --> Store the sensor related in String format
	 */
	public SensorData JsonToSensorData(String jsondata) {
		SensorData sensorData;
		Gson gson = new Gson();
		sensorData = gson.fromJson(jsondata, SensorData.class);
		return sensorData;
	}

}
