/*
Created on February 14, 2019
@author: Paavan Gopala
 */

package schooldomain.studentname.connecteddevices.labs.module05;

import java.util.logging.Logger;
import com.labbenchstudios.edu.connecteddevices.common.BaseDeviceApp;
import com.labbenchstudios.edu.connecteddevices.common.DeviceApplicationException;

public class Module05App extends BaseDeviceApp {

	// static variable
	private static final Logger _Logger = Logger.getLogger(Module05App.class.getSimpleName());

	/*
	 * @param args
	 */
	public static void main(String[] args) {
		Module05App app = new Module05App(Module05App.class.getSimpleName(), args);
		app.startApp();
	}

	/*
	 * Default Constructor
	 */
	public Module05App() {
		super();
	}

	/*
	 * Parameterized Constructor
	 * 
	 * @param appName
	 */
	public Module05App(String appName) {
		super(appName);
	}

	/*
	 * Parameterized Constructor
	 * 
	 * @param appName
	 * 
	 * @param args
	 */
	public Module05App(String appName, String[] args) {
		super(appName, args);
	}

	// protected method
	@Override
	protected void start() throws DeviceApplicationException {
		_Logger.info("Hello - module05 here!");

	}

	// protected method
	@Override
	protected void stop() throws DeviceApplicationException {
		_Logger.info("Stopping module05 app...");
	}

}
