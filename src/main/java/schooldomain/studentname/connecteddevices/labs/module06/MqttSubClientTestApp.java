package schooldomain.studentname.connecteddevices.labs.module06;

import java.util.logging.Logger;

public class MqttSubClientTestApp {
	/**
	 * class to Subscribe to a topic
	 * 
	 * @param : logger --> Logger object is used to log messages for a specific
	 *        system or application component.
	 * @param: app --> reference to this class
	 * @param: mqqtClient --> reference to MqttClientConnector
	 */
	private static final Logger logger = Logger.getLogger(MqttSubClientTestApp.class.getName());
	private static MqttSubClientTestApp app;
	private MqttClientConnector mqttClient;

	/**
	 * Default constructor
	 */
	public MqttSubClientTestApp() {

		super();

	}

	/**
	 * @param : topicName --> the name of topic to subscribe to this methods
	 *        subscribes to a topic and then unsubscribe after some time
	 */
	public void start(String topicName) {

		mqttClient = new MqttClientConnector();
		mqttClient.connect(); // Call for connect() method
		mqttClient.SubscribeToTopic(topicName); // subscribe to a topic

		try {

			Thread.sleep(30000); // sleep for 30 seconds

		}

		catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		mqttClient.unSubscibe(topicName); // unsubscribe
		mqttClient.disconnect(); // disconnect

	}

	public static void main(String[] args) {

		app = new MqttSubClientTestApp();

		try {

			app.start("Subscribe to Topic Message");

		}

		catch (Exception ex) {

			ex.printStackTrace();

		}
	}
}
